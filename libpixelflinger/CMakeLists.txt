# HEADERS: ${laaridincdir}/pixelflinger-${LAARID_API_VERSION}
# ------------------------------------------------------

set(pixelflinger_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/pixelflinger/format.h
  ${CMAKE_CURRENT_LIST_DIR}/include/pixelflinger/pixelflinger.h
)
install(
  FILES
    ${pixelflinger_HEADERS}
  DESTINATION "${laaridincdir}/pixelflinger-${LAARID_API_VERSION}/pixelflinger"
)

# LTLIBRARIES: liblaarid-pixelflinger0
# -------------------------------

set(pixelflinger_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/fixed.cpp
  ${CMAKE_CURRENT_LIST_DIR}/picker.cpp
  ${CMAKE_CURRENT_LIST_DIR}/pixelflinger.cpp
  ${CMAKE_CURRENT_LIST_DIR}/trap.cpp
  ${CMAKE_CURRENT_LIST_DIR}/scanline.cpp

  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/ARMAssemblerInterface.cpp
  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/ARMAssemblerProxy.cpp
  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/CodeCache.cpp
  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/GGLAssembler.cpp
  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/load_store.cpp
  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/blending.cpp
  ${CMAKE_CURRENT_LIST_DIR}/codeflinger/texturing.cpp
  ${CMAKE_CURRENT_LIST_DIR}/format.cpp
  ${CMAKE_CURRENT_LIST_DIR}/clear.cpp
  ${CMAKE_CURRENT_LIST_DIR}/raster.cpp
  ${CMAKE_CURRENT_LIST_DIR}/buffer.cpp
)
if(${CPU_ARM})
  list(APPEND pixelflinger_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/ARMAssembler.cpp
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/disassem.c
    ${CMAKE_CURRENT_LIST_DIR}/col32cb16blend.S
    ${CMAKE_CURRENT_LIST_DIR}/t32cb16blend.S
  )
elseif(${CPU_ARM64})
  list(APPEND pixelflinger_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/arch-arm64/col32cb16blend.S
    ${CMAKE_CURRENT_LIST_DIR}/arch-arm64/t32cb16blend.S
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/Arm64Assembler.cpp
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/Arm64Disassembler.cpp
  )
elseif(${CPU_MIPS})
  list(APPEND pixelflinger_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/arch-mips/t32cb16blend.S
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/MIPSAssembler.cpp
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/mips_disassem.c
  )
elseif(${CPU_MIPS64})
  list(APPEND pixelflinger_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/arch-mips64/col32cb16blend.S
    ${CMAKE_CURRENT_LIST_DIR}/arch-mips64/t32cb16blend.S
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/MIPS64Assembler.cpp
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/MIPSAssembler.cpp
    ${CMAKE_CURRENT_LIST_DIR}/codeflinger/mips64_disassem.c
  )
endif()

add_library(pixelflinger SHARED
  ${pixelflinger_SOURCES}
)
set_target_properties(pixelflinger
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridPixelFlinger
    OUTPUT_NAME laarid-pixelflinger
)
target_include_directories(pixelflinger
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/pixelflinger-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}>
)
target_compile_definitions(pixelflinger
  PRIVATE
    -DNDEBUG
)
target_compile_options(pixelflinger
  PRIVATE
    -Wall -Werror
    -fstrict-aliasing
    -fomit-frame-pointer
    -Wno-unused-function
)
target_link_libraries(pixelflinger
  PRIVATE
    Laarid::LaaridCUtils
    Laarid::LaaridLog
    Laarid::LaaridUtils
)

install(
  TARGETS
    pixelflinger
  EXPORT pixelflinger-targets
  LIBRARY
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridPixelFlinger ALIAS pixelflinger)

# pkgconfig
# ---------

configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/laarid-pixelflinger-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-pixelflinger-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(
  FILES
    "${PROJECT_BINARY_DIR}/pkgconfig/laarid-pixelflinger-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(
  EXPORT pixelflinger-targets
  FILE LaaridPixelFlingerTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridPixelFlinger
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridPixelFlingerConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridPixelFlingerConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridPixelFlingerConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridPixelFlinger
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridPixelFlingerConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridPixelFlingerConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridPixelFlinger
)

export(
  EXPORT pixelflinger-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridPixelFlingerTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridPixelFlinger)
