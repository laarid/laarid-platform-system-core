# HEADERS: ${laaridincdir}/log-${LAARID_API_VERSION}
# --------------------------------------------------

configure_file(${CMAKE_CURRENT_LIST_DIR}/include/log/event_tag_map.h.in
  log/event_tag_map.h
  @ONLY
)

set(log_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_event_list.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_id.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_main.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/logprint.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_properties.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_radio.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_read.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_safetynet.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_system.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_time.h
  ${CMAKE_CURRENT_LIST_DIR}/include/log/log_transport.h

  ${CMAKE_CURRENT_BINARY_DIR}/log/event_tag_map.h
)
install(FILES ${log_HEADERS}
  DESTINATION "${laaridincdir}/log-${LAARID_API_VERSION}/log")

set(log_android_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/android/log.h
)
install(FILES ${log_android_HEADERS}
  DESTINATION "${laaridincdir}/log-${LAARID_API_VERSION}/android")

# LTLIBRARIES: liblaarid-log0
# ---------------------------

set(log_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/config_read.cpp
  ${CMAKE_CURRENT_LIST_DIR}/config_read.h
  ${CMAKE_CURRENT_LIST_DIR}/config_write.cpp
  ${CMAKE_CURRENT_LIST_DIR}/config_write.h
  ${CMAKE_CURRENT_LIST_DIR}/log_event_list.cpp
  ${CMAKE_CURRENT_LIST_DIR}/log_event_write.cpp
  ${CMAKE_CURRENT_LIST_DIR}/log_portability.h
  ${CMAKE_CURRENT_LIST_DIR}/logger_lock.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logger_name.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logger_read.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logger_write.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logprint.cpp
  ${CMAKE_CURRENT_LIST_DIR}/stderr_write.cpp

  ${CMAKE_CURRENT_LIST_DIR}/event_tag_map.cpp
  ${CMAKE_CURRENT_LIST_DIR}/include/private/android_logger.h
  ${CMAKE_CURRENT_LIST_DIR}/log_time.cpp
  ${CMAKE_CURRENT_LIST_DIR}/properties.cpp
  ${CMAKE_CURRENT_LIST_DIR}/pmsg_reader.cpp
  ${CMAKE_CURRENT_LIST_DIR}/pmsg_writer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logd_reader.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logd_reader.h
  ${CMAKE_CURRENT_LIST_DIR}/logd_writer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/logger.h
)

add_library(log SHARED ${log_SOURCES})
set_target_properties(log
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridLog
    OUTPUT_NAME laarid-log
)
target_include_directories(log
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/log-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/libsystem/include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
  INTERFACE
    $<TARGET_PROPERTY:Laarid::LaaridBionic,INTERFACE_INCLUDE_DIRECTORIES>
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/base/include
    ${CMAKE_CURRENT_SOURCE_DIR}/libcutils/include
    ${CMAKE_CURRENT_SOURCE_DIR}/libutils/include
)
target_compile_features(log
  PRIVATE
    cxx_std_17 # for std::string_view
)
target_compile_definitions(log
  PRIVATE
    -D__LAARID__
    -DNDEBUG

    # This is what we want to do:
    #  liblog_cflags := $(shell \
    #   sed -n \
    #       's/^\([0-9]*\)[ \t]*liblog[ \t].*/-DLIBLOG_LOG_TAG=\1/p' \
    #       $(LOCAL_PATH)/event.logtags)
    # so make sure we do not regret hard-coding it as follows:
    -DLIBLOG_LOG_TAG=1006
    -DSNET_EVENT_LOG_TAG=1397638484
)
target_compile_options(log
  PRIVATE
    -Wall -Werror
)
target_link_libraries(log
  PRIVATE
    ${CMAKE_THREAD_LIBS_INIT}
    Laarid::LaaridBionic

    # TODO: This is to work around b/24465209. Remove after root cause is fixed
    $<$<BOOL:${CPU_ARM}>:-Wl,--hash-style=both>
)

install(TARGETS log
  EXPORT log-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridLog ALIAS log)

# pkgconfig
# ---------

configure_file("${CMAKE_CURRENT_LIST_DIR}/laarid-log-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-log-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(FILES "${PROJECT_BINARY_DIR}/pkgconfig/laarid-log-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(EXPORT log-targets
  FILE LaaridLogTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridLog
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridLogConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridLogConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridLogConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridLog
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridLogConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridLogConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridLog
)

export(EXPORT log-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridLogTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridLog)
