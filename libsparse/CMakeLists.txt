# HEADERS: ${laaridincdir}/sparse-${LAARID_API_VERSION}
# -----------------------------------------------------

set(sparse_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/sparse/sparse.h
)
install(FILES ${sparse_HEADERS}
  DESTINATION "${laaridincdir}/sparse-${LAARID_API_VERSION}/sparse")

# LTLIBRARIES: liblaarid-sparse0
# ------------------------------

set(sparse_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/backed_block.cpp
  ${CMAKE_CURRENT_LIST_DIR}/backed_block.h
  ${CMAKE_CURRENT_LIST_DIR}/defs.h
  ${CMAKE_CURRENT_LIST_DIR}/output_file.cpp
  ${CMAKE_CURRENT_LIST_DIR}/output_file.h
  ${CMAKE_CURRENT_LIST_DIR}/sparse.cpp
  ${CMAKE_CURRENT_LIST_DIR}/sparse_crc32.cpp
  ${CMAKE_CURRENT_LIST_DIR}/sparse_crc32.h
  ${CMAKE_CURRENT_LIST_DIR}/sparse_defs.h
  ${CMAKE_CURRENT_LIST_DIR}/sparse_file.h
  ${CMAKE_CURRENT_LIST_DIR}/sparse_format.h
  ${CMAKE_CURRENT_LIST_DIR}/sparse_err.cpp
  ${CMAKE_CURRENT_LIST_DIR}/sparse_read.cpp
)

add_library(sparse SHARED ${sparse_SOURCES})
set_target_properties(sparse
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridSparse
    OUTPUT_NAME laarid-sparse
)
target_include_directories(sparse
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/sparse-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
  PRIVATE
    ${ZLIB_INCLUDE_DIRS}
)
target_compile_definitions(sparse
  PRIVATE
    -DNDEBUG
)
target_compile_options(sparse
  PRIVATE
    -Wall -Werror
)
target_link_libraries(sparse
  PRIVATE
    ${ZLIB_LIBRARIES}
    Laarid::LaaridBase
)

install(TARGETS sparse
  EXPORT sparse-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridSparse ALIAS sparse)

# pkgconfig
# ---------

configure_file("${CMAKE_CURRENT_LIST_DIR}/laarid-sparse-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-sparse-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(FILES "${PROJECT_BINARY_DIR}/pkgconfig/laarid-sparse-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(EXPORT sparse-targets
  FILE LaaridSparseTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridSparse
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSparseConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridSparseConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSparseConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridSparse
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSparseConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSparseConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridSparse
)

export(EXPORT sparse-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSparseTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridSparse)

# bin_PROGRAMS: append2simg
# -------------------------

add_executable(append2simg
  ${CMAKE_CURRENT_LIST_DIR}/append2simg.cpp
)
target_compile_options(append2simg
  PRIVATE
    -Wall -Werror
)
target_link_libraries(append2simg
  PRIVATE
    Laarid::LaaridSparse
)
install(TARGETS append2simg
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# bin_PROGRAMS: img2simg
# ----------------------

add_executable(img2simg
  ${CMAKE_CURRENT_LIST_DIR}/img2simg.cpp
)
target_compile_options(img2simg
  PRIVATE
    -Wall -Werror
)
target_link_libraries(img2simg
  PRIVATE
    Laarid::LaaridSparse
)
install(TARGETS img2simg
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# bin_PROGRAMS: simg2img
# ----------------------

add_executable(simg2img
  ${CMAKE_CURRENT_LIST_DIR}/simg2img.cpp
)
target_compile_options(simg2img
  PRIVATE
    -Wall -Werror
)
target_link_libraries(simg2img
  PRIVATE
    Laarid::LaaridSparse
)
install(TARGETS simg2img
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# bin_SCRIPTS: simg_dump.py
# -------------------------

install(PROGRAMS ${CMAKE_CURRENT_LIST_DIR}/simg_dump.py
  DESTINATION ${CMAKE_INSTALL_BINDIR}
)
