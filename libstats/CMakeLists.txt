# HEADERS: ${laaridincdir}/statssocket-${LAARID_API_VERSION}
# ------------------------------------------------------

set(statssocket_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/stats_event_list.h
)
install(
  FILES
    ${statssocket_HEADERS}
  DESTINATION "${laaridincdir}/statssocket-${LAARID_API_VERSION}"
)

# LTLIBRARIES: liblaarid-statssocket0
# -------------------------------

add_library(statssocket SHARED
  ${CMAKE_CURRENT_LIST_DIR}/stats_event_list.c
  ${CMAKE_CURRENT_LIST_DIR}/statsd_writer.c
)
set_target_properties(statssocket
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridStatsSocket
    OUTPUT_NAME laarid-statssocket
)
target_include_directories(statssocket
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/statssocket-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>

    $<TARGET_PROPERTY:Laarid::LaaridLog,INTERFACE_INCLUDE_DIRECTORIES>

  PRIVATE
    $<TARGET_PROPERTY:Laarid::LaaridCUtils,INTERFACE_INCLUDE_DIRECTORIES>
)
target_compile_definitions(statssocket
  PRIVATE
    -DNDEBUG
    -DLIBLOG_LOG_TAG=1006
    -DWRITE_TO_STATSD=1
    -DWRITE_TO_LOGD=0
)
target_compile_options(statssocket
  PRIVATE
    -Wall -Werror
)
target_link_libraries(statssocket
  PRIVATE
    ${CMAKE_THREAD_LIBS_INIT}
    Laarid::LaaridBionic
)

install(
  TARGETS
    statssocket
  EXPORT statssocket-targets
  LIBRARY
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridStatsSocket ALIAS statssocket)

# pkgconfig
# ---------

configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/laarid-statssocket-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-statssocket-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(
  FILES
    "${PROJECT_BINARY_DIR}/pkgconfig/laarid-statssocket-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(
  EXPORT statssocket-targets
  FILE LaaridStatsSocketTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridStatsSocket
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridStatsSocketConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridStatsSocketConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridStatsSocketConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridStatsSocket
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridStatsSocketConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridStatsSocketConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridStatsSocket
)

export(
  EXPORT statssocket-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridStatsSocketTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridStatsSocket)
