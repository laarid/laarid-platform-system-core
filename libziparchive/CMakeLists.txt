# HEADERS: ${laaridincdir}/ziparchive-${LAARID_API_VERSION}
# ----------------------------------------------------------------

set(ziparchive_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/ziparchive/zip_archive.h
  ${CMAKE_CURRENT_LIST_DIR}/include/ziparchive/zip_archive_stream_entry.h
  ${CMAKE_CURRENT_LIST_DIR}/include/ziparchive/zip_writer.h
)
install(FILES ${ziparchive_HEADERS}
  DESTINATION "${laaridincdir}/ziparchive-${LAARID_API_VERSION}/ziparchive")

# LTLIBRARIES: liblaarid-ziparchive0
# -----------------------------------------

set(ziparchive_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/entry_name_utils-inl.h
  ${CMAKE_CURRENT_LIST_DIR}/zip_archive.cc
  ${CMAKE_CURRENT_LIST_DIR}/zip_archive_common.h
  ${CMAKE_CURRENT_LIST_DIR}/zip_archive_private.h
  ${CMAKE_CURRENT_LIST_DIR}/zip_archive_stream_entry.cc
  ${CMAKE_CURRENT_LIST_DIR}/zip_writer.cc
)

add_library(ziparchive SHARED ${ziparchive_SOURCES})
set_target_properties(ziparchive
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridZipArchive
    OUTPUT_NAME laarid-ziparchive
)
target_compile_features(ziparchive
  PRIVATE
    cxx_std_17 # for std::string_view
)
target_include_directories(ziparchive
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/ziparchive-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
  INTERFACE
    $<TARGET_PROPERTY:Laarid::LaaridBase,INTERFACE_INCLUDE_DIRECTORIES>
    $<TARGET_PROPERTY:Laarid::LaaridUtils,INTERFACE_INCLUDE_DIRECTORIES>
)
target_compile_definitions(ziparchive
  PRIVATE
    -DZLIB_CONST
    -DNDEBUG
)
target_compile_options(ziparchive
  PRIVATE
    -Wall -Werror
)
target_link_libraries(ziparchive
  PRIVATE
    ${ZLIB_LIBRARIES}
    Laarid::LaaridBase
    Laarid::LaaridLog
)

install(TARGETS ziparchive
  EXPORT ziparchive-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridZipArchive ALIAS ziparchive)

# pkgconfig
# ---------

configure_file("${CMAKE_CURRENT_LIST_DIR}/laarid-ziparchive-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-ziparchive-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(FILES "${PROJECT_BINARY_DIR}/pkgconfig/laarid-ziparchive-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(EXPORT ziparchive-targets
  FILE LaaridZipArchiveTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridZipArchive
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridZipArchiveConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridZipArchiveConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridZipArchiveConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridZipArchive
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridZipArchiveConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridZipArchiveConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridZipArchive
)

export(EXPORT ziparchive-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridZipArchiveTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridZipArchive)

# bin_PROGRAMS: ziparchive-unzip
# ------------------------------

add_executable(ziparchive-unzip
  ${CMAKE_CURRENT_LIST_DIR}/unzip.cpp
)
target_compile_definitions(ziparchive-unzip
  PRIVATE
    -DZLIB_CONST
)
target_compile_options(ziparchive-unzip
  PRIVATE
    -Wall -Werror
)
target_link_libraries(ziparchive-unzip
  PRIVATE
    Laarid::LaaridBase
    Laarid::LaaridZipArchive
)
install(TARGETS ziparchive-unzip
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# GTest
# -----

if(WITH_GTEST)
  # ziparchive-tests
  # ----------------

  set(ziparchive-tests_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/entry_name_utils_test.cc
    ${CMAKE_CURRENT_LIST_DIR}/zip_archive_test.cc
    ${CMAKE_CURRENT_LIST_DIR}/zip_writer_test.cc
  )

  add_executable(ziparchive-tests ${ziparchive-tests_SOURCES})
  target_compile_definitions(ziparchive-tests
    PRIVATE
      -DZLIB_CONST
  )
  target_compile_options(ziparchive-tests
    PRIVATE
      -Wall -Werror
  )
  target_link_libraries(ziparchive-tests
    PRIVATE
      ${CMAKE_THREAD_LIBS_INIT}
      ${GTEST_BOTH_LIBRARIES}
      Laarid::LaaridBase
      Laarid::LaaridUtils
      Laarid::LaaridZipArchive
  )

  file(COPY ${CMAKE_CURRENT_LIST_DIR}/testdata
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
  gtest_add_tests(TARGET ziparchive-tests)

  # ziparchive-benchmarks
  # ---------------------

  add_executable(ziparchive-benchmarks
    ${CMAKE_CURRENT_LIST_DIR}/zip_archive_benchmark.cpp
  )
  target_compile_definitions(ziparchive-benchmarks
    PRIVATE
      -DZLIB_CONST
  )
  target_compile_options(ziparchive-benchmarks
    PRIVATE
      -Wall -Werror
  )
  target_link_libraries(ziparchive-benchmarks
    PRIVATE
      benchmark
      Laarid::LaaridBase
      Laarid::LaaridZipArchive
  )

  # google-benchmark throws std::out_of_range on s390x.
  # See bug: laarid/laarid-platform-system-core#15
  if(NOT ${CMAKE_SYSTEM_PROCESSOR} STREQUAL "s390x")
    add_test(NAME ziparchive.Benchmarks
      COMMAND ziparchive-benchmarks)
  endif()

endif()
