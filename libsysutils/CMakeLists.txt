# HEADERS: ${laaridincdir}/sysutils-${LAARID_API_VERSION}
# -------------------------------------------------------

set(sysutils_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/FrameworkCommand.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/FrameworkListener.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/NetlinkEvent.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/NetlinkListener.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/ServiceManager.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/SocketClientCommand.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/SocketClient.h
  ${CMAKE_CURRENT_LIST_DIR}/include/sysutils/SocketListener.h
)
install(FILES ${sysutils_HEADERS}
  DESTINATION "${laaridincdir}/sysutils-${LAARID_API_VERSION}/sysutils")

# LTLIBRARIES: liblaarid-sysutils0
# --------------------------------

set(sysutils_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/src/FrameworkCommand.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/FrameworkListener.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/NetlinkEvent.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/NetlinkListener.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/ServiceManager.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/SocketClient.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/SocketListener.cpp
)

add_library(sysutils SHARED ${sysutils_SOURCES})
set_target_properties(sysutils
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridSysUtils
    OUTPUT_NAME laarid-sysutils
)
target_include_directories(sysutils
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/sysutils-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
  INTERFACE
    $<TARGET_PROPERTY:Laarid::LaaridCUtils,INTERFACE_INCLUDE_DIRECTORIES>
)
target_compile_definitions(sysutils
  PUBLIC
    -D__LAARID__

  PRIVATE
    -DNDEBUG
)
target_compile_options(sysutils
  PRIVATE
    -Wall -Werror
)
target_link_libraries(sysutils
  PRIVATE
    ${CMAKE_THREAD_LIBS_INIT}
    Laarid::LaaridBase
    Laarid::LaaridCUtils
    Laarid::LaaridLog
)

install(TARGETS sysutils
  EXPORT sysutils-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridSysUtils ALIAS sysutils)

# pkgconfig
# ---------

configure_file("${CMAKE_CURRENT_LIST_DIR}/laarid-sysutils-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-sysutils-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(FILES "${PROJECT_BINARY_DIR}/pkgconfig/laarid-sysutils-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(EXPORT sysutils-targets
  FILE LaaridSysUtilsTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridSysUtils
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSysUtilsConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridSysUtilsConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSysUtilsConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridSysUtils
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSysUtilsConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSysUtilsConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridSysUtils
)

export(EXPORT sysutils-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridSysUtilsTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridSysUtils)
