# HEADERS: ${laaridincdir}/utils-${LAARID_API_VERSION}
# ----------------------------------------------------

set(utils_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/AndroidThreads.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Atomic.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/BitSet.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/ByteOrder.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/CallStack.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Compat.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Condition.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Debug.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Endian.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Errors.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/FastStrcmp.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/FileMap.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Flattenable.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Functor.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/JenkinsHash.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/KeyedVector.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/LightRefBase.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/List.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Log.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Looper.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/LruCache.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/misc.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Mutex.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/NativeHandle.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Printer.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/ProcessCallStack.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/PropertyMap.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/RefBase.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/RWLock.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Singleton.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/SortedVector.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/StopWatch.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/String16.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/String8.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/StrongPointer.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/SystemClock.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/ThreadDefs.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Thread.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/threads.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Timers.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Tokenizer.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Trace.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/TypeHelpers.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Unicode.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/Vector.h
  ${CMAKE_CURRENT_LIST_DIR}/include/utils/VectorImpl.h
)
install(FILES ${utils_HEADERS}
  DESTINATION "${laaridincdir}/utils-${LAARID_API_VERSION}/utils")

# LTLIBRARIES: liblaarid-utils0
# -----------------------------

set(utils_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/FileMap.cpp
  ${CMAKE_CURRENT_LIST_DIR}/JenkinsHash.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Looper.cpp
  ${CMAKE_CURRENT_LIST_DIR}/NativeHandle.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Printer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/PropertyMap.cpp
  ${CMAKE_CURRENT_LIST_DIR}/RefBase.cpp
  ${CMAKE_CURRENT_LIST_DIR}/SharedBuffer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/SharedBuffer.h
  ${CMAKE_CURRENT_LIST_DIR}/StopWatch.cpp
  ${CMAKE_CURRENT_LIST_DIR}/String16.cpp
  ${CMAKE_CURRENT_LIST_DIR}/String8.cpp
  ${CMAKE_CURRENT_LIST_DIR}/StrongPointer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/SystemClock.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Threads.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Timers.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Tokenizer.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Trace.cpp
  ${CMAKE_CURRENT_LIST_DIR}/Unicode.cpp
  ${CMAKE_CURRENT_LIST_DIR}/VectorImpl.cpp
  ${CMAKE_CURRENT_LIST_DIR}/misc.cpp
)

add_library(utils SHARED ${utils_SOURCES})
set_target_properties(utils
  PROPERTIES
    VERSION ${LAARID_LT_VERSION}
    SOVERSION ${LAARID_MAJOR_VERSION}
    EXPORT_NAME LaaridUtils
    OUTPUT_NAME laarid-utils
)
target_include_directories(utils
  PUBLIC
    $<INSTALL_INTERFACE:${laaridincdir}/utils-${LAARID_API_VERSION}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>

    $<TARGET_PROPERTY:Laarid::LaaridBase,INTERFACE_INCLUDE_DIRECTORIES>
    $<TARGET_PROPERTY:Laarid::LaaridCUtils,INTERFACE_INCLUDE_DIRECTORIES>
    $<TARGET_PROPERTY:Laarid::LaaridLog,INTERFACE_INCLUDE_DIRECTORIES>
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/libbacktrace/include
)
target_compile_definitions(utils
  PUBLIC
    -D__LAARID__
  PRIVATE
    -DNDEBUG
)
target_compile_options(utils
  PRIVATE
    -Wall -Werror

    # /usr/bin/ld: CMakeFiles/utils.dir/libutils/Looper.cpp.o: relocation R_X86_64_PC32 against protected symbol `_ZN7android6Looper16threadDestructorEPv' can not be used when making a shared object
    $<0:-fvisibility=protected>
)
target_link_libraries(utils
  PRIVATE
    ${CMAKE_THREAD_LIBS_INIT}
    ${CMAKE_DL_LIBS}
    Laarid::LaaridBionic
    Laarid::LaaridCUtils
    Laarid::LaaridLog
    Laarid::LaaridProcessGroup
    Laarid::LaaridVndkSupport
)

install(TARGETS utils
  EXPORT utils-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

#Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Laarid::LaaridUtils ALIAS utils)

# pkgconfig
# ---------

configure_file("${CMAKE_CURRENT_LIST_DIR}/laarid-utils-${LAARID_API_VERSION}.pc.in"
  "${PROJECT_BINARY_DIR}/pkgconfig/laarid-utils-${LAARID_API_VERSION}.pc"
  @ONLY
)
install(FILES "${PROJECT_BINARY_DIR}/pkgconfig/laarid-utils-${LAARID_API_VERSION}.pc"
  DESTINATION "${pkgconfigdir}"
)

# CMake
# -----

install(EXPORT utils-targets
  FILE LaaridUtilsTargets.cmake
  NAMESPACE Laarid::
  DESTINATION ${laaridcmakedir}/LaaridUtils
)

#Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridUtilsConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/LaaridUtilsConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridUtilsConfig.cmake
  INSTALL_DESTINATION ${laaridcmakedir}/LaaridUtils
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridUtilsConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridUtilsConfigVersion.cmake
  DESTINATION ${laaridcmakedir}/LaaridUtils
)

export(EXPORT utils-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/cmake/LaaridUtilsTargets.cmake
  NAMESPACE Laarid::
)

export(PACKAGE LaaridUtils)

if(WITH_GTEST)

  # libutils_tests_singleton1
  # -------------------------

  add_library(libutils_tests_singleton1 SHARED
    ${CMAKE_CURRENT_LIST_DIR}/Singleton_test1.cpp
  )
  target_compile_options(libutils_tests_singleton1
    PRIVATE
      -Wall -Werror
  )
  target_include_directories(libutils_tests_singleton1
    PRIVATE
      ${GTEST_INCLUDE_DIRS}
      ${CMAKE_CURRENT_SOURCE_DIR}/base/include
      ${CMAKE_CURRENT_SOURCE_DIR}/libcutils/include
      ${CMAKE_CURRENT_SOURCE_DIR}/libutils/include
  )

  # libutils_tests_singleton2
  # -------------------------

  add_library(libutils_tests_singleton2 SHARED
    ${CMAKE_CURRENT_LIST_DIR}/Singleton_test2.cpp
  )
  target_compile_options(libutils_tests_singleton2
    PRIVATE
      -Wall -Werror
  )
  target_include_directories(libutils_tests_singleton2
    PRIVATE
      ${GTEST_INCLUDE_DIRS}
      ${CMAKE_CURRENT_SOURCE_DIR}/base/include
      ${CMAKE_CURRENT_SOURCE_DIR}/libcutils/include
      ${CMAKE_CURRENT_SOURCE_DIR}/libutils/include
  )
  target_link_libraries(libutils_tests_singleton2
    PRIVATE
      libutils_tests_singleton1
  )

  # TEST: libutils_test
  # -------------------

  set(libutils_test_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/BitSet_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/FileMap_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/LruCache_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Mutex_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/SharedBuffer_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/String8_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/StrongPointer_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/SystemClock_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Unicode_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Vector_test.cpp

    # Known failures:
    # See https://gitlab.com/laarid/laarid-platform-system-core/issues/11
    # ----------------------------------------------------------
    #${CMAKE_CURRENT_LIST_DIR}/Singleton_test.cpp
  )

  add_executable(libutils_test ${libutils_test_SOURCES})
  target_compile_options(libutils_test
    PRIVATE
      -Wall -Werror -Wextra
      $<$<CXX_COMPILER_ID:Clang>:-Wthread-safety>
  )
  target_link_libraries(libutils_test
    PRIVATE
      ${CMAKE_THREAD_LIBS_INIT}
      ${CMAKE_DL_LIBS}
      Laarid::LaaridBase
      Laarid::LaaridCUtils
      Laarid::LaaridLog
      Laarid::LaaridUtils
      ${GTEST_BOTH_LIBRARIES}
  )

  gtest_add_tests(TARGET libutils_test)

endif()
